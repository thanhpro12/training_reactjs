import axios from 'axios';
const KEY = 'AIzaSyAreJTk8ZMuChp2tcAMfFL9oqckG4cDHcg';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3/',
    params: {
        part: 'snippet',
        key: KEY
    }
})