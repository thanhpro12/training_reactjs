import React from 'react';
import PlayList from './PlayList/index'
import Home from './Home'
import '../style/video.css';
import history from './utilhistory'
import {
  Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

class App extends React.Component {

  constructor(props) {
    super(props);
  }
  componentWillMount(){
    let user = JSON.parse(localStorage.getItem('user')) || {};
    this.setState({ user })
  }
  setUser=(user)=>{
    this.setState({
      user
    })
    localStorage.setItem('user', JSON.stringify(user))
  }
  logout=()=>{
    this.setState({
      user:{}
    })
    localStorage.setItem('user', JSON.stringify({}))
  }
  state = {
    user:{}
  };
  render() {
    return (
      <Router history={history}>
        
          <Switch>
            <Route path="/playList/:id">
              <PlayList user={this.state.user}/>
            </Route>
            <Route path="/">
              <Home  user={this.state.user} setUser={this.setUser} logout={this.logout}/>
            </Route>
          </Switch>
      </Router>
    );
  }
}

export default App;
