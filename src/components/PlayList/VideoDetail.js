import React from 'react';
import ReactPlayer from 'react-player'
import { Line, Circle } from 'rc-progress';

class VideoDetail extends React.Component {
    constructor(props){
        super(props);
    }
    state = {
        playedSeconds:0,
        duration:0,
        percent:0,
    };
    updateTime=(...res)=>{
        this.setState({
            percent:res[0].played,
            playedSeconds:Math.round(res[0].playedSeconds),
            duration:Math.round(res[0].playedSeconds/res[0].played)
        })
        
    }
    timeFormater=(time)=>{
        let minute=Math.round(time/60)
        let second=Math.round(time%60)
        minute=minute<10?'0'+minute:minute
        second=second<10?'0'+second:second
        return `${minute}:${second}`
    }
    render(){
    let {handleEndVideo,video,isPlaying}=this.props
    if (!video) {
        return <div>Loading ...</div>;
    }
    return (
        <div>
            {this.props.mediaMode?<div className="circle-contain">
            <Circle percent={this.state.percent*100} strokeWidth="3" strokeColor="#4cbb17" />
    <div className='time-contain'>{this.timeFormater(this.state.playedSeconds)}/<span className="mini-time">{this.timeFormater(this.state.duration)}</span></div>
                </div>:''}
                <div className='ui embed' style={this.props.mediaMode?{display:'none'}:{}}>
                <ReactPlayer
                    url={`https://www.youtube.com/embed/${video.videoId}`}
                    playing={isPlaying}
                    onEnded={()=>handleEndVideo(video.videoId)}
                    onProgress={this.updateTime}
                    controls
                />
            </div>
            <div className='ui segment'>
                <h4 className='ui header'>{video.videoTitle}</h4>
                <p>{video.videoDescription}</p>
            </div>
        </div>

    )
    }
}

export default VideoDetail;