import React from 'react';
import VideoItem from './VideoItem';

const VideoList = ({videos , handleVideoSelect, selectedVideo,deleteVideo}) => {
    const renderedVideos = videos.map((video) => {
        const isSelected=selectedVideo?video.videoId==selectedVideo.videoId:false
        return <VideoItem key={video.videoId} video={video} handleVideoSelect={handleVideoSelect} isSelected={isSelected}  deleteVideo={deleteVideo} />
    })

    return <div className= 'playlist-wrap'>
    <div className="title-list">Danh sách phát</div>
    <div className='ui relaxed divided list video-list-wrap'>{renderedVideos}</div>
    </div>;
};
export default VideoList;