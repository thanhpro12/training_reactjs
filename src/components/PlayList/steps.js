import React from 'react';
import history from '../utilhistory'

class Steps extends React.Component {
    constructor(props){
        super(props);
    }
    state = {
        
    };
    navigateHome=()=>{
        history.push(`/`);
    }
    render() {
        return (
            <div className="ui steps">
                    <div onClick={this.navigateHome} className="link active step">
                        <i className="home icon"></i>
                        <div className="content">
                            <div className="title">Trang chủ</div>
                            <div className="description">Danh sách cá nhân và danh sách được chia sẻ</div>
                        </div>
                    </div>
                    <div className="link active step">
                    <i className="list icon"></i>
                        <div className="content">
                            <div className="title">Danh sách phát</div>
                            <div className="description">Xem các video của danh sách được chọn</div>
                        </div>
                    </div>
                </div>
        )
    }
}
export default Steps;