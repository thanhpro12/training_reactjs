import React from 'react';
import SearchBar from './Searchbar';
import youtube from '../../apis/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
import Steps from './steps'

class PlayList extends React.Component {
    constructor(props) {
        super(props)
        console.log(this.props)
    }
    state = {
        ID: '',
        videos: [],
        isPause: false,
        selectedVideo: null,
        errorMessage: '',
        isPlaying:true,
        mediaMode:false
    }
    componentWillMount() {
        let paths = window.location.pathname.split('/')
        let ID = paths[paths.length - 1]
        let playlists = JSON.parse(localStorage.getItem('playlists')) || [];
        playlists.forEach((playlist) => {
            if (playlist.ID === ID) {
                this.setState({
                    ID: playlist.ID,
                    videos: playlist.videos || [],
                    selectedVideo:playlist.videos&&playlist.videos.length?playlist.videos[0]:null,
                })
            }
        })
    }
    handleSubmit = async (termFromSearchBars) => {
        termFromSearchBars = termFromSearchBars.split(',');
        termFromSearchBars = termFromSearchBars.map((termFromSearchBar) => termFromSearchBar.split('watch?v=')[1])
        termFromSearchBars = termFromSearchBars.filter((element) => {
            if (element) {
                let flag = true
                this.state.videos.forEach((video) => { if (video.videoId == element) flag = false })
                return flag
            }
            else
                return false
        })
        if (termFromSearchBars.length) {
            let termFromSearchBar = termFromSearchBars.join()
            const response = await youtube.get('/videos', {
                params: {
                    id: termFromSearchBar
                }
            })

            let videos = response.data.items.map((item) => {
                return {
                    videoImg: item.snippet.thumbnails.medium.url,
                    videoDescription: item.snippet.description,
                    videoId: item.id,
                    videoTitle: item.snippet.title
                }
            })
            this.setState({
                videos: [...this.state.videos, ...videos],
                errorMessage: ''
            }, () => {
                let playlists = JSON.parse(localStorage.getItem('playlists')) || [];
                let { ID, videos } = this.state
                playlists = playlists.map(playlist => playlist.ID === ID ? { ...playlist, videos: videos } : playlist)
                localStorage.setItem('playlists', JSON.stringify(playlists))
            })
            if (this.state.videos.length - termFromSearchBars.length == 0) {
                this.setState({
                    selectedVideo: this.state.videos[0]
                })
            }
        }
        else {
            this.setState({
                errorMessage: 'Nhập URL không đúng hoặc trùng lặp, mời nhập lại!'
            })
        }
    };
    handleVideoSelect = (video,e) => {
        if(e.target.tagName!=='I')
            this.setState({ selectedVideo: video })
    }
    handleEndVideo = () => {
        this.nextVideo()
    }
    backVideo = () => {
        let { videos, selectedVideo } = this.state
        if (!videos) return
        videos.forEach((video, index) => {
            if (video.videoId === selectedVideo.videoId) {
                if (index == 0) {
                    this.setState({
                        selectedVideo: videos[videos.length - 1]
                    })
                }
                else
                    this.setState({
                        selectedVideo: videos[index - 1]
                    })
            }
        })
    }
    nextVideo = () => {
        let { videos, selectedVideo } = this.state
        if (!videos) return
        videos.forEach((video, index) => {
            if (video.videoId === selectedVideo.videoId) {
                if (index == videos.length - 1) {
                    this.setState({
                        selectedVideo: videos[0]
                    })
                }
                else
                    this.setState({
                        selectedVideo: videos[index + 1]
                    })
            }
        })
    }
    deleteVideo = (videoId) => {
        let updatedVideos=this.state.videos.filter((video) => video.videoId === videoId ? false : true)
        let currentVideo=this.state.selectedVideo
        let updatedVideo={...videoId==currentVideo.videoId?updatedVideos[0]:currentVideo}
        this.setState({ 
            videos: updatedVideos,
            selectedVideo:updatedVideo
        })
        let playlists = JSON.parse(localStorage.getItem('playlists')) || [];
        let { ID} = this.state
        playlists = playlists.map(playlist => playlist.ID === ID ? { ...playlist, videos: updatedVideos } : playlist)
        localStorage.setItem('playlists', JSON.stringify(playlists))
    }
    toggleVideo=()=>{
        this.setState({
            isPlaying:!this.state.isPlaying
        })
    }
    changeMode=()=>{
        this.setState({mediaMode:!this.state.mediaMode})
    }
    render() {
        let { isPause,mediaMode } = this.state
        return (

            <div className='ui container' style={{ marginTop: '1em' }}>
                <Steps />
                <div className="checkbox-mode">
                <div className="ui toggle checkbox">
                        <input
                            onChange={this.changeMode}
                            checked={mediaMode}
                            type="checkbox"
                            name="public"
                        />
                        <label>Chế độ Audio</label>
                    </div>
                    </div>
                <SearchBar handleFormSubmit={this.handleSubmit} errorMessage={this.state.errorMessage} />
                <div className='ui grid'>
                    <div className="ui row">
                        <div className="eleven wide column video-play-wrap">
                            <VideoDetail isPlaying={this.state.isPlaying} video={this.state.selectedVideo} handleEndVideo={this.handleEndVideo} mediaMode={this.state.mediaMode} />
                        </div>
                        <div className="five wide column">
                            <div className="handle-video-wrapper">
                                <i onClick={this.backVideo} className="huge backward icon cursor-poiter" style={{ marginRight: '13px' }}></i>
                                <i onClick={this.toggleVideo} className={this.state.isPlaying ? 'huge play circle icon cursor-poiter' : 'huge stop circle icon cursor-poiter'}></i>
                                <i onClick={this.nextVideo} className="huge forward icon cursor-poiter" style={{ marginLeft: '9px' }}></i>
                            </div>
                            <VideoList handleVideoSelect={this.handleVideoSelect} videos={this.state.videos} isPause={isPause} selectedVideo={this.state.selectedVideo} deleteVideo={this.deleteVideo} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PlayList;