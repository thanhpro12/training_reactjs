import React from 'react';

const VideoItem = ({video , handleVideoSelect, isSelected, deleteVideo}) => {
    return (
        <div onClick={ (e) => handleVideoSelect(video,e)} className={isSelected?'background-fb video-item item':' video-item item'} >
            <img className='ui image image-video' src={video.videoImg} alt={video.videoDescription}/>
            <div className='content'>
                <div className='header title-video'>{video.videoTitle}</div>
            </div>
            <i
                        className="close icon"
                        onClick={(e)=>deleteVideo(video.videoId,e)}
                        style={{ alignSelf: 'normal',transform:'translateX(180%)' }}
                    >
                    </i>
        </div>
    )
};
export default VideoItem;