import React from 'react';
import PlaylistCard from './PlaylistCard'
import AddPlaylistForm from './AddPlaylistForm'
import Steps from '../PlayList/steps.js'
import GoogleLogin from 'react-google-login';

class Home extends React.Component {
    state = {
        isAdding: false,
        isUserInfo:false,
        // playlists: [
        //     {
        //         ID: '1',
        //         title: '100 bài hát hay nhất của Ưng Hoàng Phúc',
        //         user: 'ThànhNV',
        //         userImg: 'https://lh4.googleusercontent.com/-p9TayFqtVpQ/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdCrwhLG2acknGEy6x87XMnTk1h7w/s96-c/photo.jpg',
        //         description: 'Tổng hợp những bài hát hay nhất của ca sĩ Ưng Hoàng Phúc',
        //         isPrivate: 'true',
        //         videos: []
        //     },
    }
    componentWillMount() {
        let playlists = JSON.parse(localStorage.getItem('playlists')) || [];
        this.setState({ playlists })
    }
    generateID = () => {
        return '_' + Math.random().toString(36).substr(2, 9);
    };
    togglePopup = () => {
        this.setState(
            { isAdding: this.state.isAdding ? false : true }
        )
    }
    closePopup = () => {
        this.setState(
            { isAdding: false }
        )
    }
    addPlaylist = (playlist) => {
        let { user } = this.props
        this.setState({
            playlists: [
                ...this.state.playlists,
                {
                    ID: this.generateID(),
                    title: playlist.name,
                    user: user.userName,
                    userImg: user.userImg,
                    description: playlist.description,
                    isPrivate: playlist.isPrivate
                }
            ]
        }, () => localStorage.setItem('playlists', JSON.stringify(this.state.playlists)))

    }
    deletePlaylist = (ID) => {
        this.setState({
            playlists: this.state.playlists.filter(playlist => playlist.ID === ID ? false : true),
        }, () => localStorage.setItem('playlists', JSON.stringify(this.state.playlists)))
    }
    successGoogle = (response) => {
        this.props.setUser({
            userName: response.profileObj.name,
            userImg: response.profileObj.imageUrl
        })

    }
    falureGoogle = (response) => {

    }
    checkPrivate=(ID)=>{
        let {playlists}=this.state
        this.setState({
            playlists:playlists.map((playlist)=>playlist.ID===ID?{...playlist,isPrivate:!playlist.isPrivate}:playlist)
        })
    }
    toggleUser=()=>{
        this.setState({isUserInfo:!this.state.isUserInfo})
    }
    userInfo=()=>{
        return <div className="html ui top attached segment user-wrapper">
        <div className="ui form ">
            <div className="required field">
                <label>Tên danh sách</label>
                
            </div>
            <div className="field">
                <label>Mô tả</label>
                
            </div>
            <div className="button-add-playlist">
                <button
                    onClick={()=>{this.props.logout();this.toggleUser()}}
                    className="ui button"
                    type="submit"
                >
                    Đăng xuất
                    </button>
            </div>
        </div>
        <div className="ui top attached label label-add">
                    Tài khoản người dùng
                </div>
        </div>
    }
    render = () => {
        let { isAdding,isUserInfo, playlists } = this.state;
        let { user } = this.props
        return (
            <div style={{ backgroundColor: '#fbfbfb' }}>
                <div className="ui inverted vertical masthead center aligned segment header-wrapper" style={{ minHeight: '700px' }}>
                    <div className="ui container" >
                        <div className="ui large secondary inverted pointing menu" style={{ border: 'none' }}>
                            <img className="youtube-img" src="https://www.pikpng.com/pngl/b/302-3020684_youtube-app-icon-png-transparent-background-youtube-social.png" />
                            <a className="active item bold-font border-none">PLAYLIST</a>
                            <div className="right item">
                                {
                                    user.userName ? <span className="user-info-wrapper"><img onClick={this.toggleUser} className="ui mini circular image image-avatar cursor-poiter" src={user.userImg}></img>{isUserInfo?this.userInfo():''}<span className='font-dancing'>{user.userName}</span> </span> : <GoogleLogin
                                        clientId="973245948043-8et9snku0dm04d9ajgc2dic5b2vtc19v.apps.googleusercontent.com"
                                        onSuccess={this.successGoogle}
                                        onFailure={this.falureGoogle}
                                        buttonText="Login Google"
                                        render={renderProps => (
                                            <a className="ui inverted button" onClick={renderProps.onClick} disabled={renderProps.disabled}>Login</a>
                                        )}
                                    />
                                }
                            </div>
                        </div>
                    </div>

                    <div className="ui text container">
                        <h1 className="ui inverted header web-title">
                            Danh sách yêu thích
                        </h1>
                        <h2 className='web-description'>Tạo danh sách từ các video trên youtube, chia sẻ cùng mọi người sở thích của bạn</h2>
                        <div onClick={()=>this.divContent.scrollIntoView({ behavior: "smooth" })} className="ui huge primary button web-description linear-back">Khám phá  <i className="right arrow icon"></i></div>
                    </div>

                </div>
                <div className='ui container' ref={(el) => { this.divContent = el; }} style={{ marginTop: '3em', padding: '4rem 6rem', backgroundColor: 'white', boxShadow: '0 1px 6px 0 rgba(32, 33, 36, .28)' }}>
                    <section className=''>
                        <Steps />
                        {user.userName ? <>
                            <div className="ui three column grid">
                                <div className="column">
                                    <h2 className='ui header myplaylist'>
                                        <i className="big list icon">
                                        </i>Danh sách nghe của tôi
                            </h2>
                                </div>
                                <div className="column" >
                                    <button className="ui labeled icon button button-add" onClick={this.togglePopup}>
                                        <i className="plus icon"></i>
                                        Thêm danh sách
                        </button>
                                </div>
                            </div>
                            {isAdding ? <AddPlaylistForm closePopup={this.closePopup} addPlaylist={this.addPlaylist} /> : ''}
                            <div className="ui three column grid">
                                {playlists.map(playlist =>playlist.user===user.userName? <PlaylistCard deletePlaylist={this.deletePlaylist} checkPrivate={this.checkPrivate} key={playlist.ID} playlist={playlist} />:'')}
                            </div>
                            <div className="pagination-playlist">
                                <div className="ui pagination menu ">
                                    <a className="active item">
                                        1
                            </a>
                                    <a className="item">
                                        2
                            </a>
                                    <div className="disabled item">
                                        ...
                            </div>
                                    <a className="item">
                                        10
                            </a>
                                    <a className="item">
                                        11
                            </a>
                                    <a className="item">
                                        12
                            </a>
                                </div>
                            </div></> : ''}
                    </section>
                    <section>
                        <div><h2 className='ui header myplaylist'><i className="big share square icon"></i>Danh sách nghe được chia sẻ</h2></div>
                        <div className="ui three column grid">
                                {playlists.map(playlist =>(playlist.user!==user.userName)&&(!playlist.isPrivate)? <PlaylistCard isShared={true} deletePlaylist={this.deletePlaylist} checkPrivate={this.checkPrivate} key={playlist.ID} playlist={playlist} />:'')}
                            </div>
                            <div className="pagination-playlist">
                                <div className="ui pagination menu ">
                                    <a className="active item">
                                        1
                            </a>
                                    <a className="item">
                                        2
                            </a>
                                    <div className="disabled item">
                                        ...
                            </div>
                                    <a className="item">
                                        10
                            </a>
                                    <a className="item">
                                        11
                            </a>
                                    <a className="item">
                                        12
                            </a>
                                </div>
                            </div>
                    </section>
                </div>
            </div>
        )
    }
}

export default Home;



