import React from 'react';


class AddPlaylistForm extends React.Component {
    constructor(props) {
        super(props);
    }
    state = {
        name: '',
        description: '',
        isPrivate: false
    }
    handleCheckboxChange = (event) => {
        this.setState({ isPrivate: event.target.checked })
    }
    handleNameChange = (event) => {
        this.setState({ name: event.target.value })
    }
    handleDescriptionChange = (event) => {
        this.setState({ description: event.target.value })
    }
    handleAddPlaylist=(event)=>{
        event.preventDefault();
        this.props.addPlaylist({...this.state})
    }
    render = () => {
        let { closePopup } = this.props
        let { name, description, isPrivate } = this.state
        return (
            <div className="html ui top attached segment addfield-wrapper">
                <div className="ui form ">
                    <div className="required field">
                        <label>Tên danh sách</label>
                        <input
                            onChange={this.handleNameChange}
                            value={name}
                            type="text"
                        />
                    </div>
                    <div className="field">
                        <label>Mô tả</label>
                        <textarea
                            onChange={this.handleDescriptionChange}
                            value={description}
                            rows="4"
                        >
                        </textarea>
                    </div>
                    <div className="ui toggle checkbox">
                        <input
                            onChange={this.handleCheckboxChange}
                            checked={isPrivate}
                            type="checkbox"
                            name="public"
                        />
                        <label>Riêng tư</label>
                    </div>
                    <div className="button-add-playlist">
                        <button
                            onClick={this.handleAddPlaylist}
                            className="ui button"
                            type="submit"
                        >
                            Thêm mới
                            </button>
                    </div>
                </div>
                <div className="ui top attached label label-add">
                    Thêm danh sách
                    <i
                        className="close icon"
                        onClick={closePopup}
                        style={{ float: 'right' }}
                    >
                    </i>
                </div>
            </div>
        )
    }
}

export default AddPlaylistForm;