import React from 'react';
import history from '../utilhistory'

class PlaylistCard extends React.Component {
    constructor(props){
        super(props);
    }
    state = {

    }
    
    navigatePlaylist=()=>{
        history.push(`/playlist/${this.props.playlist.ID}`);
    }
    render = () => {
        let {ID,title,user,userImg,description,isPrivate}=this.props.playlist
        let {deletePlaylist,checkPrivate,isShared}=this.props
        return (
            <div className="column">
            {/* <div className='playlist-card'> */}
            <div className="ui card playlist-card">
                <div className="image">
                    <img onClick={this.navigatePlaylist} src='https://cdn.icon-icons.com/icons2/584/PNG/512/play_youtube_video_media_playlist_vid_icon-icons.com_55213.png' />
                    {isShared?'':<i
                        className="times circle outline big icon delete-playlist"
                        style={{color:'red'}}
                        onClick={()=>deletePlaylist(ID)}
                    >
                    </i>}
                </div>
                <div onClick={this.navigatePlaylist} className="content">
                    <div className="header"><i className="music icon"></i>{title}</div>
                    <div className="meta">
                        <a>Playlist</a>
                    </div>
                    <div className="description">
                        {description}
          </div>
                </div>
                <div className="extra content">
                    {isShared?'':<span className="right floated">
                        <div className="ui toggle checkbox">
                            <input checked={isPrivate} onChange={()=>checkPrivate(ID)} type="checkbox" name="public" />
                            <label>Private</label>
                        </div>
                    </span>}
                    <span>
                        <a className="ui image label">
                            <img src="https://lh4.googleusercontent.com/-p9TayFqtVpQ/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdCrwhLG2acknGEy6x87XMnTk1h7w/s96-c/photo.jpg" />
                            {user}
                            </a>
                    </span>
                </div>
            </div>
            {/* </div> */}
            </div>
        )
    }
}

export default PlaylistCard;